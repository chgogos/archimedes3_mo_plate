import org.apache.log4j.Logger;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.Variable;
import org.moeaframework.core.variable.EncodingUtils;

public class Plate implements Problem {
	static Logger log = Logger.getLogger(Plate.class.getName());
	public static MatlabAgent ma;
	private int numberOfVariables;
	protected int numberOfObjectives;
	protected int[] objectives;
	private int numberOfConstraints;

	static int evaluation = 0;

	public static int getNumberOfActuators(Variable var) {
		boolean[] d = EncodingUtils.getBinary(var);
		int c = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i])
				c++;
		}
		return c;
	}

	public Plate() {
		this.numberOfVariables = 1;
		this.numberOfObjectives = 2;
		this.objectives = new int[] { 0, 1};
		this.numberOfConstraints = 1;
	}

	@Override
	public String getName() {
		return "Beam";
	}

	@Override
	public int getNumberOfVariables() {
		return numberOfVariables;
	}

	@Override
	public int getNumberOfObjectives() {
		return numberOfObjectives;
	}

	@Override
	public int getNumberOfConstraints() {
		return numberOfConstraints;
	}

	@Override
	public void evaluate(Solution solution) {
		evaluation++;
		boolean[] d = EncodingUtils.getBinary(solution.getVariable(0));

		int c = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i])
				c++;
		}
		if (c == MatlabAgent.NUMBER_OF_ACTUATORS) {
			solution.setConstraint(0, 0.0); // constraints that are satisfied
											// have a value of zero
			double[] objs = ma.getObjectives(d, objectives);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < numberOfObjectives; i++) {
				if (i == 0) {
					sb.append(String.format("Eval#%d %s [nac=%d] OBJECTIVE_0=%e ",
							evaluation, solution.getVariable(0), c, objs[i]));
				} else {
					sb.append(String.format("OBJECTIVE_%d=%e ", objectives[i],
							objs[i]));
				}
			}
			log.debug(sb.toString());
			solution.setObjectives(objs);
		} else if (c > MatlabAgent.NUMBER_OF_ACTUATORS) {
			solution.setConstraint(0, c - MatlabAgent.NUMBER_OF_ACTUATORS);
			double[] objs = new double[numberOfObjectives];
			solution.setObjectives(objs);
		} else if (c < MatlabAgent.NUMBER_OF_ACTUATORS) {
			solution.setConstraint(0, MatlabAgent.NUMBER_OF_ACTUATORS - c);
			double[] objs = new double[numberOfObjectives];
			solution.setObjectives(objs);
		}
	}

	@Override
	public Solution newSolution() {
		Solution sol = new Solution(numberOfVariables, numberOfObjectives,
				numberOfConstraints);
		sol.setVariable(0,
				EncodingUtils.newBinary(MatlabAgent.NUMBER_OF_ELEMENTS));
		return sol;
	}

	@Override
	public void close() {
		// do nothing
	}
}
