public class Pair {

	private final double obj1;
	private final double obj2;

	public Pair(double obj1, double obj2) {
		this.obj1 = obj1;
		this.obj2 = obj2;
	}

	public double getObj1() {
		return obj1;
	}

	public double getObj2() {
		return obj2;
	}

	public boolean dominates(Pair o) {
		return (this.obj1 < o.obj1) && (this.obj2 < o.obj2);
	}

}