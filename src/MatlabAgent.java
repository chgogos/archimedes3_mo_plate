import java.util.Random;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.MatlabProxyFactoryOptions;

public class MatlabAgent {

	static int NUMBER_OF_ELEMENTS = -1;
	static int NUMBER_OF_ACTUATORS = -1;

	Random r;
	MatlabProxy proxy;
	String matlabCodePath;
	String matlabMFile;

	public static void setGlobalParametes(int numberOfElements,
			int numberOfActuators) {
		NUMBER_OF_ELEMENTS = numberOfElements;
		NUMBER_OF_ACTUATORS = numberOfActuators;
	}

	public MatlabAgent(String matlabCodePath, String matlabMFile) {
		r = new Random(123456789L);
		this.matlabCodePath = matlabCodePath;
		this.matlabMFile = matlabMFile;
		MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
				.setUsePreviouslyControlledSession(true).setHidden(true)
				.setMatlabLocation(null).build();
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		try {
			proxy = factory.getProxy();
			proxy.eval("addpath('" + matlabCodePath + "')");
		} catch (MatlabConnectionException e) {
			e.printStackTrace();
		} catch (MatlabInvocationException e) {
			e.printStackTrace();
		}
	}

	public double[] getObjectives(boolean[] a, int[] obj_ids) {
		int numberOfObjectives = 0;
		for (int i = 0; i < obj_ids.length; i++) {
			if (obj_ids[i] > numberOfObjectives)
				numberOfObjectives = obj_ids[i];
		}
		numberOfObjectives++;
		double[] rv = new double[obj_ids.length];
		try {
			proxy.eval("addpath('" + matlabCodePath + "')");
			Object[] args = new Object[2];
			int[] alpha = new int[NUMBER_OF_ELEMENTS];

			for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
				alpha[i] = a[i] ? 1 : 0;
			}
			args[0] = alpha;
			args[1] = NUMBER_OF_ACTUATORS;
			Object[] ro;
			ro = proxy.returningFeval(matlabMFile, 2, args);
			int i = 0;
			for (int ri : obj_ids) {
				rv[i] = ((double[]) ro[ri])[0];
				i++;
			}
		} catch (MatlabInvocationException e) {
			e.printStackTrace();
		}
		return rv;
	}

	public void disposeMatlabEnv() {
		try {
			proxy.eval("rmpath('" + matlabCodePath + "')");
			// close connection
			proxy.disconnect();
			// proxy.exit();
		} catch (MatlabInvocationException e) {
			e.printStackTrace();
		}

	}

}
