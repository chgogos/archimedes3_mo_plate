import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;
import java.util.Vector;

//log4j
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
//joda.time
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
//moea
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.Variable;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import rcaller.RCaller;
import rcaller.RCode;

// guava
import com.google.common.base.Stopwatch;

public class UI {
	static Logger log = Logger.getLogger(UI.class.getName());
	static DateTimeFormatter fmt = DateTimeFormat
			.forPattern("yyyy_MM_dd_HH_mm");

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		UI ui = new UI();
		ui.setup();
		ui.menu();
	}

	String rExePath, matlabCodePath, matlabMFile;
	int numberOfElements, numberOfActuators;

	public void setup() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			String cpuId = MiscUtils.getMotherboardSN();
			if (cpuId.equalsIgnoreCase("101507740002010"))
				input = new FileInputStream("config.properties");
			else if (cpuId.equalsIgnoreCase("5555P010"))
				input = new FileInputStream("config_crete.properties");
			else {
				System.err.println("Unknown computer");
				System.exit(-1);
			}
			prop.load(input);
			rExePath = prop.getProperty("r_exe_path").trim();
			matlabCodePath = prop.getProperty("matlab_code_path").trim();
			matlabMFile = prop.getProperty("matlab_m_file").trim();
			numberOfElements = Integer.parseInt(prop.getProperty(
					"number_of_elements").trim());
			numberOfActuators = Integer.parseInt(prop.getProperty(
					"number_of_actuators").trim());
			log.debug("r_exe_path:              " + rExePath);
			log.debug("matlab_code_path:        " + matlabCodePath);
			log.debug("matlab_m_file:           " + matlabMFile);
			log.debug("number_of_elements:      " + numberOfElements);
			log.debug("number_of_actuators:     " + numberOfActuators);
			MatlabAgent.setGlobalParametes(numberOfElements, numberOfActuators);
			input.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private void menu() {
		Scanner in = new Scanner(System.in);
		System.out.println("1. Validate Matlab - MOEA connectivity");
		System.out.println("2. Optimization with 2 objectives");
		System.out.println("3. Enumeration");
		System.out.print("Enter choice: ");
		int choice = in.nextInt();
		if (choice == 1) {
			sanity_test();
		} else if (choice == 2) {
			System.out.print("Enter algorithm (NSGAII, SPEA2, Random):");
			String algorithm = in.next();
			System.out.print("Enter max evaluations: ");
			int maxEvaluations = in.nextInt();
			Class objectivesClass = Plate.class;
			optimize2Objectives(objectivesClass, algorithm, maxEvaluations);
		} else if (choice == 3) {
			enumerate();
		} else {
			System.exit(-1);
		}
		in.close();
	}

	private void enumerate() {
		Integer[] v = new Integer[numberOfElements];
		for (int i = 0; i < numberOfElements; i++)
			v[i] = i;
		ICombinatoricsVector<Integer> originalVector = Factory.createVector(v);
		Generator<Integer> gen = Factory.createSimpleCombinationGenerator(
				originalVector, numberOfActuators);
		MatlabAgent ma = new MatlabAgent(matlabCodePath, matlabMFile);
		int c = 0;
		Vector<Pair> results = new Vector<Pair>();
		outer_loop: for (ICombinatoricsVector<Integer> perm : gen) {
			System.out.println("Pareto front size = " + results.size());
			c++;
			if (c > 100)
				break;
			// System.out.println(perm);
			boolean[] test_sequence = new boolean[MatlabAgent.NUMBER_OF_ELEMENTS];
			for (Integer x : perm)
				test_sequence[x] = true;
			double[] r = ma.getObjectives(test_sequence, new int[] { 0, 1 });
			// System.out.printf("obj0=%e obj1=%e\n", r[0], r[1]);
			Pair pair = new Pair(r[0], r[1]);
			for (Pair p : results) {
				if (p.dominates(pair))
					continue outer_loop;
			}
			Vector<Pair> to_be_removed = new Vector<Pair>();
			for (Pair p : results) {
				if (pair.dominates(p))
					to_be_removed.add(p);
			}
			results.removeAll(to_be_removed);
			results.add(pair);
		}
		ma.disposeMatlabEnv();
		for (Pair p : results) {
			System.out
					.printf("Obj1: %e - Obj2: %e\n", p.getObj1(), p.getObj2());
		}
	}

	private void sanity_test() {
		Stopwatch timer = Stopwatch.createStarted();
		MatlabAgent ma = new MatlabAgent(matlabCodePath, matlabMFile);

		boolean[] dummy = new boolean[MatlabAgent.NUMBER_OF_ELEMENTS];

		for (int i = 0; i < MatlabAgent.NUMBER_OF_ACTUATORS; i++)
			dummy[i] = true;

		// dummy[0]=true;
		// ...
		// dummy[5]=true;

		double[] r = ma.getObjectives(dummy, new int[] { 0, 1 });
		System.out.printf("obj0=%e obj1=%e\n", r[0], r[1]);
		ma.disposeMatlabEnv();
		timer.stop();
		System.out.println("Time elapsed: " + timer.toString());
	}

	private void optimize2Objectives(Class objectivesClass, String algorithm,
			int maxEvaluations) {
		Stopwatch timer = Stopwatch.createStarted();
		Plate.ma = new MatlabAgent(matlabCodePath, matlabMFile);

		NondominatedPopulation result = new Executor()
				.withProblemClass(objectivesClass).withAlgorithm(algorithm)
				.withMaxEvaluations(maxEvaluations).run();

		double values[][] = new double[result.size()][2];
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			values[i][0] = objs[0];
			values[i][1] = objs[1];
			Variable v = solution.getVariable(0);
			String msg = String.format(
					"Solution %d %s [nac=%d] Obj0:%e Obj1:%e", i + 1,
					solution.getVariable(0), Plate.getNumberOfActuators(v),
					objs[0], objs[1]);
			log.info(msg);
		}

		Plate.ma.disposeMatlabEnv();
		timer.stop();
		log.info("Time elapsed: " + timer.toString());

		DateTime dt = new DateTime();
		String fn_no_extension = String.format("%s_%s_%d_obj0_vs_obj1",
				dt.toString(fmt), algorithm, maxEvaluations);
		String exportCSVFilename = String.format("%s.csv", fn_no_extension);
		save2ObjsCSV(result, values, exportCSVFilename);

		String exportImageFilename = String.format("%s.jpg", fn_no_extension);
		f_plot_obj0_vs_obj1(exportCSVFilename, exportImageFilename);
	}

	private void save2ObjsCSV(NondominatedPopulation result, double[][] values,
			String exportFilename) {
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFilename));
			writer.write("obj0, obj1, locations, nac\n");
			for (int i = 0; i < result.size(); i++) {
				Solution solution = result.get(i);
				Variable v = solution.getVariable(0);
				writer.write(String.format("%e, %e, %s, %d\n", values[i][0],
						values[i][1], v, Plate.getNumberOfActuators(v)));
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void f_plot_obj0_vs_obj1(String csvfilename, String filename) {
		RCaller caller = new RCaller();
		caller.setRscriptExecutable(rExePath + "/Rscript.exe");
		RCode code = new RCode();
		File file;
		try {
			file = code.startPlot();
			caller.setRCode(code);
			code.R_require("dplyr");
			code.addRCode("source('f_plot_obj0_vs_obj1.R')");
			String rcmd = String.format("f_plot_obj0_vs_obj1('%s', '%s')",
					csvfilename, filename);
			code.addRCode(rcmd);
			code.endPlot();
			caller.setRCode(code);
			caller.runOnly();
			code.showPlot(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}